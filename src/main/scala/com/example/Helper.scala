package com.example

object Helper {

  def charToCharacter(char: Char): Character = char match {
    case 'A' => CharA
    case 'B' => CharB
    case 'C' => CharC
    case 'D' => CharD
    case 'E' => CharE
    case 'F' => CharF
    case 'G' => CharG
    case 'H' => CharH
    case 'I' => CharI
    case 'J' => CharJ
    case 'K' => CharK
    case 'L' => CharL
    case 'M' => CharM
    case 'N' => CharN
    case 'O' => CharO
    case 'P' => CharP
    case 'Q' => CharQ
    case 'R' => CharR
    case 'S' => CharS
    case 'T' => CharT
    case 'U' => CharU
    case 'V' => CharV
    case 'W' => CharW
    case 'X' => CharX
    case 'Y' => CharY
    case 'Z' => CharZ
    case default => CharEmpty
  }
}