package com.example

import java.awt.{Color, Dimension}

import scala.swing.BorderPanel.Position._
import scala.swing._
import scala.swing.event.MouseClicked

object DisplayApplication extends SimpleSwingApplication {

  def textToDisplay = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"

  def top = new MainFrame { // top is a required method
    title = "A Sample Scala Swing GUI"

    val canvas = new Display(new Dimension(890, 200))

    contents = new BorderPanel {
      layout(canvas) = Center
    }

    //size = new Dimension(800, 200)
//    menuBar = new MenuBar {
//      contents += new Menu("File") {
//        contents += new MenuItem(Action("Exit") {
//          sys.exit(0)
//        })
//      }
//    }

    listenTo(canvas.mouse.clicks)

    // react to events
    reactions += {
      case MouseClicked(_, point, _, _, _) =>
        //canvas.throwDart(new Dart(point.x, point.y, Color.black))
    }
  }
}