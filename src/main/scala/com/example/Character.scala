package com.example

trait Character {
  def resolution: (Int, Int)
  def bitlist: Array[Int]
  def stringRepresentation = ""

  def scale(scale: Int): Array[Int] = {
    val newResolution = (resolution._1 * scale, resolution._2 * scale)
    val target = Array.fill(newResolution._1 * newResolution._2) {0}

    // wat?
    for (x <- 0 until resolution._1; y <- 0 until resolution._2) {
      val offset = y * scale * resolution._1 * scale + (x * scale)
      for (scaleX <- 0 until scale; scaleY <- 0 until scale) {
        val scaledOffset = offset + (scaleY * resolution._1 * scale) + scaleX
        target(scaledOffset) = bitlist(y * resolution._1 + x)
      }
    }
    target
  }

  override def toString(): String = {
    stringRepresentation
  }
}


object CharEmpty extends Character {
  def resolution = (5, 9)
  override def stringRepresentation = " "
  override def bitlist: Array[Int] = Array(
    0, 0, 0, 0, 0,
    0, 0, 0, 0, 0,
    0, 0, 0, 0, 0,
    0, 0, 0, 0, 0,
    0, 0, 0, 0, 0,
    0, 0, 0, 0, 0,
    0, 0, 0, 0, 0,
    0, 0, 0, 0, 0,
    0, 0, 0, 0, 0
  )
}


object CharA extends Character {
  def resolution = (5, 9)
  override def stringRepresentation = "A"
  override def bitlist: Array[Int] = Array(
    0, 1, 1, 1, 0,
    1, 0, 0, 0, 1,
    1, 0, 0, 0, 1,
    1, 0, 0, 0, 1,
    1, 0, 0, 0, 1,
    1, 1, 1, 1, 1,
    1, 0, 0, 0, 1,
    1, 0, 0, 0, 1,
    1, 0, 0, 0, 1
  )
}


object CharB extends Character {
  def resolution = (5, 9)
  override def stringRepresentation = "B"
  override def bitlist: Array[Int] = Array(
    1, 1, 1, 1, 0,
    1, 0, 0, 0, 1,
    1, 0, 0, 0, 1,
    1, 0, 0, 0, 1,
    1, 1, 1, 1, 0,
    1, 0, 0, 0, 1,
    1, 0, 0, 0, 1,
    1, 0, 0, 0, 1,
    1, 1, 1, 1, 0
  )
}


object CharC extends Character {
  def resolution = (5, 9)
  override def stringRepresentation = "C"
  override def bitlist: Array[Int] = Array(
    0, 0, 1, 1, 1,
    0, 1, 0, 0, 0,
    1, 0, 0, 0, 0,
    1, 0, 0, 0, 0,
    1, 0, 0, 0, 0,
    1, 0, 0, 0, 0,
    1, 0, 0, 0, 0,
    0, 1, 0, 0, 0,
    0, 0, 1, 1, 1
  )
}


object CharD extends Character {
  def resolution = (5, 9)
  override def stringRepresentation = "D"
  override def bitlist: Array[Int] = Array(
    1, 1, 1, 0, 0,
    1, 0, 0, 1, 0,
    1, 0, 0, 0, 1,
    1, 0, 0, 0, 1,
    1, 0, 0, 0, 1,
    1, 0, 0, 0, 1,
    1, 0, 0, 0, 1,
    1, 0, 0, 1, 0,
    1, 1, 1, 0, 0
  )
}


object CharE extends Character {
  def resolution = (5, 9)
  override def stringRepresentation = "E"
  override def bitlist: Array[Int] = Array(
    1, 1, 1, 1, 1,
    1, 0, 0, 0, 0,
    1, 0, 0, 0, 0,
    1, 0, 0, 0, 0,
    1, 1, 1, 1, 1,
    1, 0, 0, 0, 0,
    1, 0, 0, 0, 0,
    1, 0, 0, 0, 0,
    1, 1, 1, 1, 1
  )
}


object CharF extends Character {
  def resolution = (5, 9)
  override def stringRepresentation = "F"
  override def bitlist: Array[Int] = Array(
    1, 1, 1, 1, 1,
    1, 0, 0, 0, 0,
    1, 0, 0, 0, 0,
    1, 0, 0, 0, 0,
    1, 1, 1, 1, 1,
    1, 0, 0, 0, 0,
    1, 0, 0, 0, 0,
    1, 0, 0, 0, 0,
    1, 0, 0, 0, 0
  )
}


object CharG extends Character {
  def resolution = (5, 9)
  override def stringRepresentation = "G"
  override def bitlist: Array[Int] = Array(
    0, 1, 1, 1, 0,
    1, 0, 0, 0, 1,
    1, 0, 0, 0, 0,
    1, 0, 0, 0, 0,
    1, 0, 1, 1, 1,
    1, 0, 0, 0, 1,
    1, 0, 0, 0, 1,
    1, 0, 0, 0, 1,
    0, 1, 1, 1, 0
  )
}

object CharH extends Character {
  def resolution = (5, 9)
  override def stringRepresentation = "H"
  override def bitlist: Array[Int] = Array(
    1, 0, 0, 0, 1,
    1, 0, 0, 0, 1,
    1, 0, 0, 0, 1,
    1, 0, 0, 0, 1,
    1, 1, 1, 1, 1,
    1, 0, 0, 0, 1,
    1, 0, 0, 0, 1,
    1, 0, 0, 0, 1,
    1, 0, 0, 0, 1
  )
}

object CharI extends Character {
  def resolution = (5, 9)
  override def stringRepresentation = "I"
  override def bitlist: Array[Int] = Array(
    1, 1, 1, 1, 1,
    0, 0, 1, 0, 0,
    0, 0, 1, 0, 0,
    0, 0, 1, 0, 0,
    0, 0, 1, 0, 0,
    0, 0, 1, 0, 0,
    0, 0, 1, 0, 0,
    0, 0, 1, 0, 0,
    1, 1, 1, 1, 1
  )
}


object CharJ extends Character {
  def resolution = (5, 9)
  override def stringRepresentation = "J"
  override def bitlist: Array[Int] = Array(
    0, 1, 1, 1, 1,
    0, 0, 0, 1, 0,
    0, 0, 0, 1, 0,
    0, 0, 0, 1, 0,
    0, 0, 0, 1, 0,
    0, 0, 0, 1, 0,
    0, 0, 0, 1, 0,
    1, 0, 0, 1, 0,
    0, 1, 1, 0, 0
  )
}


object CharK extends Character {
  def resolution = (5, 9)
  override def stringRepresentation = "K"
  override def bitlist: Array[Int] = Array(
    1, 0, 0, 1, 1,
    1, 0, 0, 1, 0,
    1, 0, 1, 0, 0,
    1, 0, 1, 0, 0,
    1, 1, 0, 0, 0,
    1, 0, 1, 0, 0,
    1, 0, 1, 0, 0,
    1, 0, 0, 1, 0,
    1, 0, 0, 1, 1
  )
}


object CharL extends Character {
  def resolution = (5, 9)
  override def stringRepresentation = "L"
  override def bitlist: Array[Int] = Array(
    1, 1, 0, 0, 0,
    0, 1, 0, 0, 0,
    0, 1, 0, 0, 0,
    0, 1, 0, 0, 0,
    0, 1, 0, 0, 0,
    0, 1, 0, 0, 0,
    0, 1, 0, 0, 0,
    0, 1, 0, 0, 1,
    1, 1, 1, 1, 1
  )
}

object CharM extends Character {
  def resolution = (5, 9)
  override def stringRepresentation = "M"
  override def bitlist: Array[Int] = Array(
    1, 0, 0, 0, 1,
    1, 0, 0, 0, 1,
    1, 1, 0, 1, 1,
    1, 1, 1, 1, 1,
    1, 0, 1, 0, 1,
    1, 0, 0, 0, 1,
    1, 0, 0, 0, 1,
    1, 0, 0, 0, 1,
    1, 0, 0, 0, 1
  )
}

object CharN extends Character {
  def resolution = (5, 9)
  override def stringRepresentation = "N"
  override def bitlist: Array[Int] = Array(
    1, 0, 0, 0, 1,
    1, 0, 0, 0, 1,
    1, 1, 0, 0, 1,
    1, 1, 0, 0, 1,
    1, 0, 1, 0, 1,
    1, 0, 0, 1, 1,
    1, 0, 0, 1, 1,
    1, 0, 0, 0, 1,
    1, 0, 0, 0, 1
  )
}


object CharO extends Character {
  def resolution = (5, 9)
  override def stringRepresentation = "O"
  override def bitlist: Array[Int] = Array(
    0, 1, 1, 1, 0,
    1, 0, 0, 0, 1,
    1, 0, 0, 0, 1,
    1, 0, 0, 0, 1,
    1, 0, 0, 0, 1,
    1, 0, 0, 0, 1,
    1, 0, 0, 0, 1,
    1, 0, 0, 0, 1,
    0, 1, 1, 1, 0
  )
}


object CharP extends Character {
  def resolution = (5, 9)
  override def stringRepresentation = "P"
  override def bitlist: Array[Int] = Array(
    1, 1, 1, 1, 0,
    1, 0, 0, 0, 1,
    1, 0, 0, 0, 1,
    1, 0, 0, 0, 1,
    1, 1, 1, 1, 0,
    1, 0, 0, 0, 0,
    1, 0, 0, 0, 0,
    1, 0, 0, 0, 0,
    1, 0, 0, 0, 0
  )
}


object CharQ extends Character {
  def resolution = (5, 9)
  override def stringRepresentation = "Q"
  override def bitlist: Array[Int] = Array(
    0, 1, 1, 1, 0,
    1, 0, 0, 0, 1,
    1, 0, 0, 0, 1,
    1, 0, 0, 0, 1,
    1, 0, 0, 0, 1,
    1, 0, 0, 0, 1,
    1, 0, 0, 0, 1,
    1, 0, 0, 1, 0,
    0, 1, 1, 0, 1
  )
}


object CharR extends Character {
  def resolution = (5, 9)
  override def stringRepresentation = "R"
  override def bitlist: Array[Int] = Array(
    1, 1, 1, 1, 0,
    1, 0, 0, 0, 1,
    1, 0, 0, 0, 1,
    1, 0, 0, 0, 1,
    1, 1, 1, 1, 0,
    1, 0, 1, 0, 0,
    1, 0, 0, 1, 0,
    1, 0, 0, 1, 0,
    1, 0, 0, 1, 1
  )
}


object CharS extends Character {
  def resolution = (5, 9)
  override def stringRepresentation = "S"
  override def bitlist: Array[Int] = Array(
    0, 1, 1, 1, 0,
    1, 0, 0, 0, 1,
    1, 0, 0, 0, 0,
    1, 0, 0, 0, 0,
    0, 1, 1, 1, 0,
    0, 0, 0, 0, 1,
    0, 0, 0, 0, 1,
    1, 0, 0, 0, 1,
    0, 1, 1, 1, 0
  )
}


object CharT extends Character {
  def resolution = (5, 9)
  override def stringRepresentation = "T"
  override def bitlist: Array[Int] = Array(
    1, 1, 1, 1, 1,
    1, 0, 1, 0, 1,
    0, 0, 1, 0, 0,
    0, 0, 1, 0, 0,
    0, 0, 1, 0, 0,
    0, 0, 1, 0, 0,
    0, 0, 1, 0, 0,
    0, 0, 1, 0, 0,
    0, 1, 1, 1, 0
  )
}


object CharU extends Character {
  def resolution = (5, 9)
  override def stringRepresentation = "U"
  override def bitlist: Array[Int] = Array(
    1, 0, 0, 0, 1,
    1, 0, 0, 0, 1,
    1, 0, 0, 0, 1,
    1, 0, 0, 0, 1,
    1, 0, 0, 0, 1,
    1, 0, 0, 0, 1,
    1, 0, 0, 0, 1,
    1, 0, 0, 0, 1,
    0, 1, 1, 1, 0
  )
}


object CharV extends Character {
  def resolution = (5, 9)
  override def stringRepresentation = "V"
  override def bitlist: Array[Int] = Array(
    1, 0, 0, 0, 1,
    1, 0, 0, 0, 1,
    1, 0, 0, 0, 1,
    1, 0, 0, 0, 1,
    1, 0, 0, 0, 1,
    1, 0, 0, 0, 1,
    1, 0, 0, 0, 1,
    0, 1, 0, 1, 0,
    0, 0, 1, 0, 0
  )
}


object CharW extends Character {
  def resolution = (5, 9)
  override def stringRepresentation = "W"
  override def bitlist: Array[Int] = Array(
    1, 0, 0, 0, 1,
    1, 0, 0, 0, 1,
    1, 0, 0, 0, 1,
    1, 0, 0, 0, 1,
    1, 0, 1, 0, 1,
    1, 1, 1, 1, 1,
    1, 1, 0, 1, 1,
    1, 0, 0, 0, 1,
    1, 0, 0, 0, 1
  )
}


object CharX extends Character {
  def resolution = (5, 9)
  override def stringRepresentation = "X"
  override def bitlist: Array[Int] = Array(
    1, 0, 0, 0, 1,
    1, 0, 0, 0, 1,
    0, 1, 0, 1, 0,
    0, 1, 0, 1, 0,
    0, 0, 1, 0, 0,
    0, 1, 0, 1, 0,
    0, 1, 0, 1, 0,
    1, 0, 0, 0, 1,
    1, 0, 0, 0, 1
  )
}


object CharY extends Character {
  def resolution = (5, 9)
  override def stringRepresentation = "Y"
  override def bitlist: Array[Int] = Array(
    1, 0, 0, 0, 1,
    1, 0, 0, 0, 1,
    0, 1, 0, 1, 0,
    0, 1, 0, 1, 0,
    0, 0, 1, 0, 0,
    0, 0, 1, 0, 0,
    0, 0, 1, 0, 0,
    0, 0, 1, 0, 0,
    0, 1, 1, 1, 0
  )
}


object CharZ extends Character {
  def resolution = (5, 9)
  override def stringRepresentation = "Z"
  override def bitlist: Array[Int] = Array(
    1, 1, 1, 1, 1,
    1, 0, 0, 0, 1,
    0, 0, 0, 1, 0,
    0, 0, 0, 1, 0,
    0, 0, 1, 0, 0,
    0, 1, 0, 0, 0,
    0, 1, 0, 0, 0,
    1, 0, 0, 0, 1,
    1, 1, 1, 1, 1
  )
}