package com.example

import java.awt.image.{BufferedImageOp, BufferedImage}

import scala.swing.{Dimension, Panel}
import java.awt.{ Color, Graphics2D }

case class Group(size:(Int, Int)) {
  val pixelsImage = new BufferedImage(size._1, size._2, BufferedImage.TYPE_BYTE_BINARY)
  val initImage = Array.fill(size._1 * size._2) { 0 }
  //update(initImage)

  def update(data:Array[Int]):Unit = {
    pixelsImage.getRaster.setPixels(0, 0, size._1, size._2, data)
  }
}


class Display(prefSize:Dimension) extends Panel {
  val groupSize = (50, 90)
  val xGapSize = 20
  val yGapSize = 20
  var groupsInACol = (prefSize.getWidth.toInt + xGapSize) / (groupSize._1 + xGapSize)
  val groupsInARow = (prefSize.getHeight.toInt + yGapSize) / (groupSize._2 + yGapSize)
  val displaySize = (groupsInACol, groupsInARow)

  val groups = (for(
    x <- 0 until displaySize._1;
    y <- 0 until displaySize._2
  ) yield ((x, y), Group(groupSize))).toMap

  preferredSize_=(prefSize)

  override def paintComponent(g: Graphics2D) {
    g.clearRect(0, 0, size.width, size.height)
    g.setColor(Color.BLACK)
    g.fillRect(0, 0, size.width, size.height)
    g.setColor(Color.WHITE)

    groups.foreach {
      case ((x, y), group) =>

        val pos = x + y * groupsInACol
        if(DisplayApplication.textToDisplay.length() > pos) {
          val char = DisplayApplication.textToDisplay.charAt(pos)
          group.update(Helper.charToCharacter(char).scale(10))
        }

        var _gapSizeX, _gapSizeY = 0
        if(x > 0) _gapSizeX = xGapSize
        if(y > 0) _gapSizeY = yGapSize

        val posX = x * (groupSize._1 + _gapSizeX)
        val posY = y * (groupSize._2 + _gapSizeY)

        g.drawImage(group.pixelsImage, posX, posY, this.peer)
    }
  }
}